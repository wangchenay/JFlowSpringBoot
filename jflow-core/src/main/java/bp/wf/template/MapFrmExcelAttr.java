package bp.wf.template;
import bp.sys.MapDataAttr;

/** 
 Excel表单属性 attr
*/
public class MapFrmExcelAttr extends MapDataAttr
{
	/** 
	 临时的版本号
	*/
	public static final String TemplaterVer = "TemplaterVer";
	/** 
	 文件存储字段
	*/
	public static final String DBSave = "DBSave";
}