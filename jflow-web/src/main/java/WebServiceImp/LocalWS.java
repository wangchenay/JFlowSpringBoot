package WebServiceImp;

import java.util.Hashtable;


import bp.da.DataRow;
import bp.da.DataSet;
import bp.da.DataTable;
import bp.da.DataType;
import bp.tools.Json;
import bp.wf.Flow;
import bp.wf.GenerWorkFlow;
import bp.wf.GenerWorkerListAttr;
import bp.wf.GenerWorkerLists;
import bp.wf.Node;
import bp.wf.Nodes;
import bp.wf.WFState;
import bp.wf.data.GERpt;

import bp.wf.template.Directions;
import bp.wf.template.FlowExt;
import bp.wf.template.FrmWorkCheck;
import bp.wf.template.Selector;
import WebService.LocalWSI;

public class LocalWS implements LocalWSI {

	/**
	 * 寰呭姙
	 * 
	 * @param userNo
	 *            鐢ㄦ埛缂栧彿
	 * @param sysNo
	 *            绯荤粺缂栧彿,涓虹┖鏃惰繑鍥炲钩鍙版墍鏈夋暟鎹�
	 * @return
	 * @throws Exception 
	 */
	@Override
	public String DB_Todolist(String userNo, String sysNo) throws Exception {
		try {
			bp.wf.Dev2Interface.Port_Login(userNo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String sql = "";
		if (DataType.IsNullOrEmpty(sysNo) == true)
			sql = "SELECT * FROM WF_EmpWorks WHERE FK_Emp='" + userNo + "'";
		else
			sql = "SELECT * FROM WF_EmpWorks WHERE Domain='" + sysNo + "' AND FK_Emp='" + userNo + "'";

		DataTable dt = bp.da.DBAccess.RunSQLReturnTable(sql);
		return Json.ToJson(dt);
	}

	/**
	 * 鑾峰緱鍦ㄩ��
	 * 
	 * @param userNo
	 *            鐢ㄦ埛缂栧彿
	 * @param sysNo
	 *            绯荤粺缂栧彿,涓虹┖鏃惰繑鍥炲钩鍙版墍鏈夋暟鎹�
	 * @return
	 * @throws Exception
	 */
	@Override
	public String DB_Runing(String userNo, String sysNo) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		DataTable dt = bp.wf.Dev2Interface.DB_GenerRuning(userNo, null, false);
		return bp.tools.Json.ToJson(dt);
	}

	/**
	 * 鎴戝彲浠ュ彂璧风殑娴佺▼
	 * 
	 * @param userNo
	 *            鐢ㄦ埛缂栧彿
	 * @param sysNo
	 *            绯荤粺缂栧彿,涓虹┖鏃惰繑鍥炲钩鍙版墍鏈夋暟鎹�
	 * @return 杩斿洖鎴戝彲浠ュ彂璧风殑娴佺▼鍒楄〃.
	 * @throws Exception
	 */

	@Override
	public String DB_StarFlows(String userNo, String domain) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		DataTable dt = bp.wf.Dev2Interface.DB_StarFlows(userNo, domain);
		return bp.tools.Json.ToJson(dt);
	}

	/**
	 * 鎴戝彂璧风殑娴佺▼瀹炰緥
	 * 
	 * @param userNo
	 *            鐢ㄦ埛缂栧彿
	 * @param sysNo
	 *            缁熺紪鍙�,涓虹┖鏃惰繑鍥炲钩鍙版墍鏈夋暟鎹�
	 * @param pageSize
	 * @param pageIdx
	 * @return
	 * @throws Exception 
	 */
	@Override
	public String DB_MyStartFlowInstance(String userNo, String domain, int pageSize, int pageIdx) throws Exception {
		try {
			bp.wf.Dev2Interface.Port_Login(userNo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String sql = "";
		if (domain == null)
			sql = "SELECT * FROM WF_GenerWorkFlow WHERE Starter='" + userNo + "'";
		else
			sql = "SELECT * FROM WF_GenerWorkFlow WHERE Domain='" + domain + "' AND Starter='" + userNo + "'";

		DataTable dt = bp.da.DBAccess.RunSQLReturnTable(sql);
		return bp.tools.Json.ToJson(dt);
	}

	/**
	 * 鍒涘缓WorkID
	 * 
	 * @param flowNo
	 *            娴佺▼缂栧彿
	 * @param userNo
	 *            宸ヤ綔浜哄憳缂栧彿
	 * @return 涓�涓暱鏁村瀷鐨勫伐浣滄祦绋嬪疄渚�
	 * @throws Exception
	 */

	@Override
	public long CreateWorkID(String flowNo, String userNo) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		return bp.wf.Dev2Interface.Node_CreateBlankWork(flowNo, userNo);
	}

	/**
	 * 鎵ц鍙戦��
	 * 
	 * @param flowNo
	 *            娴佺殑绋嬫ā鐗圛D
	 * @param workid
	 *            宸ヤ綔ID
	 * @param ht
	 *            鍙傛暟锛屾垨鑰呰〃鍗曞瓧娈�.
	 * @param toNodeID
	 *            鍒拌揪鐨勮妭鐐笽D.濡傛灉璁╃郴缁熻嚜鍔ㄨ绠楀氨浼犲叆0
	 * @param toEmps
	 *            鍒拌揪鐨勪汉鍛業Ds,姣斿:zhangsan,lisi,wangwu. 濡傛灉涓篘ull灏辨爣璇嗚绯荤粺鑷姩璁＄畻
	 * @return 鍙戦�佺殑缁撴灉淇℃伅.
	 * @throws Exception
	 */

	@Override
	public String SendWork(String flowNo, long workid, Hashtable ht, int toNodeID, String toEmps, String userNo)
			throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		bp.wf.SendReturnObjs objs = bp.wf.Dev2Interface.Node_SendWork(flowNo, workid, ht, toNodeID, toEmps);
        
		String msg = objs.ToMsgOfText();
        System.out.println(msg);
		Hashtable myht = new Hashtable();
		myht.put("Message", msg);
		myht.put("IsStopFlow", objs.getIsStopFlow());

		if (objs.getIsStopFlow() == false) {
			myht.put("VarAcceptersID", objs.getVarAcceptersID() == null ? "" : objs.getVarAcceptersID());
			myht.put("VarAcceptersName", objs.getVarAcceptersName() == null ? "" : objs.getVarAcceptersName());
			myht.put("VarToNodeID", objs.getVarToNodeID());
			myht.put("VarToNodeName", objs.getVarToNodeName() == null ? "" : objs.getVarToNodeName());
		}
		return bp.tools.Json.ToJson(myht);
	}

	/**
	 * 淇濆瓨鍙傛暟
	 * 
	 * @param workid
	 *            宸ヤ綔ID
	 * @param paras
	 *            鐢ㄤ簬鎺у埗娴佺▼杩愯浆鐨勫弬鏁帮紝姣斿鏂瑰悜鏉′欢. 鏍煎紡涓�:@JinE=1000@QingJaiTianShu=100
	 * @throws Exception
	 */

	@Override
	public void SaveParas(long workid, String paras, String userNo) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		bp.wf.Dev2Interface.Flow_SaveParas(workid, paras);

	}

	/**
	 * 鑾峰緱涓嬩竴涓妭鐐逛俊鎭�
	 * 
	 * @param flowNo
	 *            娴佺▼缂栧彿
	 * @param workid
	 *            娴佺▼瀹炰緥
	 * @param paras
	 *            鏂瑰悜鏉′欢鎵�闇�瑕佺殑鍙傛暟锛屽彲浠ヤ负绌恒��
	 * @return 涓嬩竴涓妭鐐圭殑JSON.
	 * @throws Exception
	 */

	@Override
	public String GenerNextStepNode(String flowNo, long workid, String paras, String userNo) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		if (paras != null)
			bp.wf.Dev2Interface.Flow_SaveParas(workid, paras);

		int nodeID = bp.wf.Dev2Interface.Node_GetNextStepNode(flowNo, workid);
		bp.wf.Node nd = new bp.wf.Node(nodeID);

		// 濡傛灉瀛楁 DeliveryWay = 4 灏辫〃绀哄埌杈剧殑鎺ョ偣鏄敱褰撳墠鑺傜偣鍙戦�佷汉閫夋嫨鎺ユ敹浜�.
		// 鑷畾涔夊弬鏁扮殑瀛楁鏄� SelfParas, DeliveryWay
		// CondModel = 鏂瑰悜鏉′欢璁＄畻瑙勫垯.
		return nd.ToJson();
	}

	/**
	 * 鑾峰緱涓嬩竴姝ヨ妭鐐圭殑鎺ユ敹浜�
	 * 
	 * @param flowNo
	 *            娴佺▼ID
	 * @param toNodeID
	 *            鑺傜偣ID
	 * @param workid
	 *            宸ヤ綔浜嬩緥ID
	 * @return 杩斿洖涓や釜缁撴灉闆嗕竴涓槸鍒嗙粍鐨凞epts(No,Name)锛屽彟澶栦竴涓槸浜哄憳鐨凟mps(No, Name,
	 *         FK_Dept),鎺ュ彈鍚庯紝鐢ㄤ簬鏋勯�犱汉鍛橀�夋嫨鍣�.
	 * @throws Exception
	 */

	@Override
	public String GenerNextStepNodeEmps(String flowNo, int toNodeID, int workid, String userNo) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		Selector select = new Selector(toNodeID);
		Node nd = new Node(toNodeID);

		GERpt rpt = new GERpt("ND" + Integer.parseInt(flowNo) + "Rpt", workid);
		DataSet ds = select.GenerDataSet(toNodeID, rpt);
		return bp.tools.Json.ToJson(ds);
	}

	/**
	 * 灏嗚閫�鍥炲埌鐨勮妭鐐�
	 * 
	 * @param workID
	 * @return 杩斿洖鑺傜偣闆嗗悎鐨刯son.
	 * @throws Exception
	 */

	@Override
	public String WillReturnToNodes(int workID, String userNo) throws Exception {

		try {

			bp.wf.Dev2Interface.Port_Login(userNo);

			GenerWorkFlow gwf = new GenerWorkFlow(workID);

			DataTable dt = bp.wf.Dev2Interface.DB_GenerWillReturnNodes(gwf.getFK_Node(), workID, gwf.getFID());
			return bp.tools.Json.ToJson(dt);
		} catch (Exception ex) {
			return "err@" + ex.getMessage();
		}
	}

	/**
	 * 灏嗚杈惧埌鐨勮妭鐐�
	 * 
	 * @param currNodeID
	 *            褰撳墠鑺傜偣ID
	 * @return 杩斿洖鑺傜偣闆嗗悎鐨刯son.
	 * @throws Exception
	 */

	@Override
	public String WillToNodes(int currNodeID, String userNo) throws Exception {

		try {
			bp.wf.Dev2Interface.Port_Login(userNo);
			Node nd = new Node(currNodeID);

			Directions dirs = new Directions();
			Nodes nds = dirs.GetHisToNodes(currNodeID, false);
			return nds.ToJson();
		} catch (Exception ex) {
			return "err@" + ex.getMessage();
		}
	}

	/**
	 * 鑾峰緱褰撳墠鑺傜偣淇℃伅.
	 * 
	 * @param currNodeID
	 *            褰撳墠鑺傜偣ID
	 * @return
	 * @throws Exception
	 */

	@Override
	public String CurrNodeInfo(int currNodeID, String userNo) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		Node nd = new Node(currNodeID);
		return nd.ToJson();
	}

	/**
	 * 鑾峰緱褰撳墠娴佺▼淇℃伅.
	 * 
	 * @param flowNo
	 *            娴佺▼ID
	 * @return 褰撳墠鑺傜偣淇℃伅
	 * @throws Exception
	 */

	@Override
	public String CurrFlowInfo(String flowNo, String userNo) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		Flow fl = new Flow(flowNo);
		return fl.ToJson();
	}

	/**
	 * 鑾峰緱褰撳墠娴佺▼淇℃伅.
	 * 
	 * @param workID
	 *            娴佺▼ID
	 * @return 褰撳墠鑺傜偣淇℃伅
	 * @throws Exception
	 */
	@Override
	public String CurrGenerWorkFlowInfo(long workID, String userNo) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		GenerWorkFlow gwf = new GenerWorkFlow(workID);
		return gwf.ToJson();
	}

	/**
	 * 閫�鍥�.
	 * 
	 * @param workID
	 *            娴佺▼ID
	 * @param retunrnToNodeID
	 *            娴佺▼閫�鍥炵殑鑺傜偣ID
	 * @param returnMsg
	 *            閫�鍥炲師鍥�
	 * @return 閫�鍥炵粨鏋滀俊鎭�
	 * @throws Exception
	 */
	@Override
	public String Node_ReturnWork(long workID, int returnToNodeID, String returnMsg, String userNo) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		GenerWorkFlow gwf = new GenerWorkFlow(workID);
		return bp.wf.Dev2Interface.Node_ReturnWork(gwf.getFK_Flow(), workID, gwf.getFID(), gwf.getFK_Node(),
				returnToNodeID, null, returnMsg, false);

	}

	/**
	 * 鎵ц娴佺▼缁撴潫 璇存槑:寮哄埗娴佺▼缁撴潫.
	 * 
	 * @param flowNo
	 *            娴佺▼缂栧彿
	 * @param workID
	 *            宸ヤ綔ID
	 * @param msg
	 *            娴佺▼缁撴潫鍘熷洜
	 * @return 杩斿洖鎴愬姛鎵ц淇℃伅
	 * @throws Exception
	 */

	@Override
	public String Flow_DoFlowOverQiangZhi(String flowNo, long workID, String msg, String userNo) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		return bp.wf.Dev2Interface.Flow_DoFlowOver(workID, msg,1);

	}

	@Override
	public void Port_Login(String userNo) throws Exception {

		bp.wf.Dev2Interface.Port_Login(userNo);
	}

	/**
	 * 鎵ц鎾ら攢
	 * 
	 * @param flowNo
	 *            娴佺▼缂栫爜
	 * @param workID
	 *            宸ヤ綔ID
	 * @param unSendToNode
	 *            鎾ら攢鍒扮殑鑺傜偣
	 * @param fid
	 * @return
	 * @throws Exception
	 */
	@Override
	public String Runing_UnSend(String userNo, String flowNo, long workID, int unSendToNode, long fid)
			throws Exception {

		bp.wf.Dev2Interface.Port_Login(userNo);

		return bp.wf.Dev2Interface.Flow_DoUnSend(flowNo, workID, unSendToNode, fid);
	}

	/**
	 * 娴佺▼缁撴潫鍚庡洖婊�
	 * 
	 * @param flowNo
	 *            娴佺▼缂栫爜
	 * @param workId
	 *            宸ヤ綔ID
	 * @param backToNodeID
	 *            鍥炴粴鍒扮殑鑺傜偣ID
	 * @param backMsg
	 *            鍥炴粴鍘熷洜
	 * @return 鍥炴粴淇℃伅
	 * @throws Exception
	 */
	@Override
	public String DoRebackFlowData(String flowNo, long workId, int backToNodeID, String backMsg, String userNo)
			throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		FlowExt flow = new FlowExt(flowNo);
		return flow.DoRebackFlowData(workId, backToNodeID, backMsg);
	}

	/**
	 * 鑾峰緱褰撳墠娴佺▼淇℃伅.
	 * 
	 * @param flowNo
	 *            娴佺▼ID.
	 * @return 褰撳墠鑺傜偣淇℃伅
	 */
	@Override
	public String CurrFlowInfo(String flowNo) throws Exception {
		Flow fl = new Flow(flowNo);
		return fl.ToJson();
	}

	/**
	 * 鑾峰緱褰撳墠娴佺▼淇℃伅.
	 * 
	 * @param flowNo
	 *            娴佺▼ID.
	 * @return 褰撳墠鑺傜偣淇℃伅
	 */
	@Override
	public String CurrGenerWorkFlowInfo(long workID) throws Exception {
		GenerWorkFlow gwf = new GenerWorkFlow(workID);
		return gwf.ToJson();
	}

	/**
	 * 鑾峰緱宸ヤ綔杩涘害-鐢ㄤ簬灞曠ず娴佺▼鐨勮繘搴﹀浘
	 * 
	 * @param workID
	 *            workID
	 * @param userNo
	 *            鐢ㄦ埛缂栧彿
	 * @return 杩斿洖寰呭姙
	 */
	@Override
	public String WorkProgressBar(long workID, String userNo) throws Exception {
		DataSet ds = bp.wf.Dev2Interface.DB_JobSchedule(workID);
		return bp.tools.Json.ToJson(ds);
	}

	/**
	 * 鑾峰緱宸ヤ綔杩涘害-鐢ㄤ簬灞曠ず娴佺▼鐨勮繘搴﹀浘 - for zhongkeshuguang.
	 * 
	 * @param workID
	 *            workID
	 * @param userNo
	 *            鐢ㄦ埛缂栧彿
	 * @return 杩斿洖寰呭姙
	 */
	@Override
	public String WorkProgressBar20(long  workID, String userNo) throws Exception
    {
		
		bp.wf.Dev2Interface.Port_Login(userNo);
		
		return bp.wf.AppClass.JobSchedule(workID);
		 
    }

	@Override
	public String SDK_Page_Init(long  workID, String userNo) throws Exception
    {
		bp.wf.Dev2Interface.Port_Login(userNo);
		return  bp.wf.Dev2Interface.SDK_Page_Init(workID);
    }

	// 鏍规嵁褰撳墠鑺傜偣鑾峰緱涓嬩竴涓妭鐐�.
	@Override
	public int GetNextNodeID(int nodeID, DataTable dirs)
    {
        int toNodeID = 0;
        for (DataRow dir : dirs.Rows)        
        {
            if ( Integer.parseInt(dir.getValue("Node").toString()) == nodeID)
            {
                toNodeID = Integer.parseInt( dir.getValue("ToNode").toString());
                break;
            }
        }

        int toNodeID2 = 0;
        
        for (DataRow dir11 : dirs.Rows)
        {
            if (Integer.parseInt(dir11.getValue("Node").toString()) ==nodeID )
            {
                toNodeID2 = Integer.parseInt(dir11.getValue("ToNode").toString());
            }
        }

        //涓ゆ鍘荤殑涓嶄竴鑷达紝灏辨湁鍒嗘敮锛屾湁鍒嗘敮灏眗eutrn 0 .
        if (toNodeID2 == toNodeID)       
            return toNodeID; 
        return  0 ; 
    }
 
	/**
	 * 鏌ヨ鏁版嵁
	 * 
	 * @param sqlOfSelect
	 *            瑕佹煡璇㈢殑sql
	 * @param password
	 *            鐢ㄦ埛瀵嗙爜
	 * @return 杩斿洖鏌ヨ鏁版嵁
	 * @throws Exception 
	 */
	@Override
	public String DB_RunSQLReturnJSON(String sqlOfSelect, String password) throws Exception {
		if (password.equals(password) == false)
			return "err@瀵嗙爜閿欒";

		DataTable dt = bp.da.DBAccess.RunSQLReturnTable(sqlOfSelect);
		return bp.tools.Json.ToJson(dt);
	}

	/**
	 * 鎵ц鎶勯��
	 * 
	 * @param flowNo
	 *            娴佺▼缂栧彿
	 * @param workID
	 *            宸ヤ綔ID
	 * @param toEmpNo
	 *            鎶勯�佷汉鍛樼紪鍙�
	 * @param toEmpName
	 *            鎶勯�佷汉鍛樹汉鍛樺悕绉�
	 * @param msgTitle
	 *            鏍囬
	 * @param msgDoc
	 *            鍐呭
	 * @return 鎵ц淇℃伅
	 * @throws Exception
	 */
	@Override
	public String Node_CC_WriteTo_CClist(int fk_node, long workID, String toEmpNo, String toEmpName, String msgTitle,
			String msgDoc, String userNo) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		return bp.wf.Dev2Interface.Node_CC_WriteTo_CClist(fk_node, workID, toEmpNo, toEmpName, msgTitle, msgDoc);
	}


	   /** 
	 	 鏄惁鍙互鏌ョ湅璇ユ祦绋�	 
	 	 @param flowNo 娴佺▼缂栧彿
	 	 @param workid 宸ヤ綔ID
	 	 @return 鏄惁鍙互鏌ョ湅璇ュ伐浣�.
	 * @throws Exception 
	 	*/
	@Override
    public Boolean Flow_IsCanView(String flowNo, long workid, String userNo) throws Exception
    {
        return bp.wf.Dev2Interface.Flow_IsCanViewTruck(flowNo, workid,userNo);
    }
    
    /** 
	 鏄惁鍙互鏌ョ湅璇ユ祦绋�	 
	 @param flowNo 瑕佹煡璇㈢殑 sql
	 @param workid 鐢ㄦ埛瀵嗙爜
	 @return 鏄惁鍙互鏌ョ湅璇ュ伐浣�.
* @throws Exception 
	*/
	@Override
    public Boolean Flow_IsCanDoCurrentWork(long workid, String userNo) throws Exception
    {
        return bp.wf.Dev2Interface.Flow_IsCanDoCurrentWork(workid, userNo);
    }

    

	/**
	 * 鑾峰彇鎸囧畾浜哄憳鐨勬妱閫佸垪琛� 璇存槑:鍙互鏍规嵁杩欎釜鍒楄〃鐢熸垚鎸囧畾鐢ㄦ埛鐨勬妱閫佹暟鎹�.
	 * 
	 * @param FK_Emp
	 *            浜哄憳缂栧彿,濡傛灉鏄痭ull,鍒欒繑鍥炴墍鏈夌殑.
	 * @return 杩斿洖璇ヤ汉鍛樼殑鎵�鏈夋妱閫佸垪琛�,缁撴瀯鍚岃〃WF_CCList.
	 */
	@Override
	public String DB_CCList(String userNo) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);

		DataTable dt = bp.wf.Dev2Interface.DB_CCList(userNo);
		return bp.tools.Json.ToJson(dt);
	}
	
	/** 
                    鍐欏叆瀹℃牳淇℃伅
     
     <param name="workid">workID</param>
     <param name="msg">瀹℃牳淇℃伅</param>
     * 
     */
	@Override
    public void Node_WriteWorkCheck(long workid, String msg) throws Exception
    {
        GenerWorkFlow gwf = new GenerWorkFlow(workid);
          bp.wf.Dev2Interface.WriteTrackWorkCheck(gwf.getFK_Flow(), gwf.getFK_Node(), gwf.getWorkID(), gwf.getFID(), msg,"瀹℃牳");
    }
	
	/**
    * 鑾峰彇瀹℃牳淇℃伅
    * @param FK_Flow 娴佺▼缂栧彿
    * @param FK_Node 鑺傜偣缂栧彿
    * @param workId 娴佺▼ID
    * @param fid 骞叉祦绋婭D锛堥拡瀵瑰瓙绾跨▼锛�
    * * @param isReadonly鏄惁鍙
    * @return
    * @throws Exception
    */
	@Override
	public String DB_WorkCheck(String FK_Flow, int FK_Node, long workId, long fid,boolean isReadonly) throws Exception {
		return FK_Flow;
//		DataSet ds =bp.wf.Dev2Interface.DB_WorkCheck(FK_Flow,FK_Node,workId,fid,isReadonly);
//		return bp.tools.Json.ToJson(ds);
	}

	/**
    * 鑾峰彇娴佺▼鏃堕棿杞存暟鎹�
    * @param workid
    * @param fid
    * @param fk_flow
    * @throws Exception
    */
	@Override
	public String Flow_TimeBase(long workid, long fid, String fk_flow) throws Exception {
		DataSet ds = new DataSet();

		//鑾峰彇track.
		DataTable dt = bp.wf.Dev2Interface.DB_GenerTrackTable(fk_flow, workid, fid);
		ds.Tables.add(dt);
		//鑾峰彇 WF_GenerWorkFlow
		GenerWorkFlow gwf = new GenerWorkFlow();
		gwf.setWorkID( workid) ;
		gwf.RetrieveFromDBSources();
		ds.Tables.add(gwf.ToDataTableField("WF_GenerWorkFlow"));

		if (gwf.getWFState() != WFState.Complete)
		{
			GenerWorkerLists gwls = new GenerWorkerLists();
			gwls.Retrieve(GenerWorkerListAttr.WorkID, workid);

			ds.Tables.add(gwls.ToDataTableField("WF_GenerWorkerList"));
		}
		
	    //鎶婅妭鐐瑰鏍搁厤缃俊鎭�.
		FrmWorkCheck fwc = new FrmWorkCheck(gwf.getFK_Node());
		ds.Tables.add(fwc.ToDataTableField("FrmWorkCheck"));

		//杩斿洖缁撴灉.
		return bp.tools.Json.ToJson(ds);
		
	}
	
	/**
	 * 鎴戝弬涓庣殑
	 */
	@Override
	public String DB_MyJoinFlows(String userNo) throws Exception {
		bp.wf.Dev2Interface.Port_Login(userNo);
		return userNo;
		
//		DataSet ds = bp.wf.Dev2Interface.DB_CommSearch("bp.wf.Data.MyJoinFlows");
//		return bp.tools.Json.ToJson(ds);
	}

	/***
	 * 鏍规嵁娴佺▼WorkID銆丗K_Flow鍒犻櫎娴佺▼
	 * @param userNo
	 * @param workid
	 * @param fk_flow
	 * @return
	 * @throws Exception
	 */
	@Override
	public String DeleteFlow(String userNo,long workid,String fk_flow) throws Exception{
		bp.wf.Dev2Interface.Port_Login(userNo);
		return bp.wf.Dev2Interface.Flow_DoDeleteFlowByReal( workid, true);
	}

	/**
	 * 闄勪欢涓婁紶
	 * @param fk_node 鑺傜偣缂栧彿
	 * @param fk_flow 娴佺▼缂栧彿
	 * @param workid 娴佺▼WorkID
	 * @param athNo 闄勪欢灞炴�х紪鍙�
	 * @param fk_mapData 琛ㄥ崟灞炴�х紪鍙�
	 * @param filePath 闄勪欢璺緞
	 * @param fileName 闄勪欢鍚嶇О
	 * @param sort 闄勪欢鍒嗙被
	 * @param fid 骞叉祦绋婭D
	 * @param pworkid 鐖舵祦绋婭D
	 * @return
	 * @throws Exception
	 */
	@Override
	public String CCForm_AddAth(int fk_node, String fk_flow, long workid, String athNo, String fk_mapData,
								String filePath, String fileName, String sort, long fid, long pworkid) throws Exception{
		bp.wf.Dev2Interface.CCForm_AddAth(fk_node,fk_flow,workid,athNo,fk_mapData,filePath,fileName,sort,fid,pworkid);
		return "闄勪欢浼犻�掓垚鍔�";
	}
}
